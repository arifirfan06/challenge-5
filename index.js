// Import module express
const express = require("express");
const path = require('path');
const app = express();

app.set('views',path.join(__dirname,'views'));
app.set("view engine", "ejs");
app.use(express.urlencoded({
  extended: false,
}));

app.use('/assets',express.static(path.join(__dirname, 'assets')));

app.get("/", (req, res) => {
  res.render("auth/login");
});

app.post("/login", (req, res) => {
  const {username, password} = req.body;
  if(username == "wert" && password == "123"){
    res.redirect("/home");
  }
  else{
    res.redirect("/");
  }
});

app.get("/home", (req, res) => {
  res.render("index");
});

// Routing untuk challenge chapter 3 ===============
app.get("/chapter3", (req, res) => {
  res.render("challenge-chapter3");
});

app.get("/chapter3/thegame", (req, res) => {
  res.render("challenge-chapter3/thegame");
});

app.get("/chapter3/features", (req, res) => {
  res.render("challenge-chapter3/features");
});

app.get("/chapter3/sysreq", (req, res) => {
  res.render("challenge-chapter3/sysreq");
});

app.get("/chapter3/topscore", (req, res) => {
  res.render("challenge-chapter3/topscore");
});

app.get("/chapter3/play-now", (req, res) => {
  res.render("challenge-chapter3/play-now");
});

app.get("/chapter3/see-more", (req, res) => {
  res.render("challenge-chapter3/see-more");
});
// ==================================================

// Routing untuk challenge chapter 4 ===============
app.get("/chapter4", (req, res) => {
  res.render("challenge-chapter4");
});

// ==================================================

app.get("/page-not-found", (req, res) => {
  res.end("404: Page Not Found");
});

const port = 3000
app.listen(port, () => {
  console.log(`Server running at port ${port}`);
});
  


